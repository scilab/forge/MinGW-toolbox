// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function bOK = mgw_cleanLibsImported()
  bOK = %f;
  scilibpath = mgw_getScilabLibPath();
  if scilibpath <> [] then
    lstlibs = findfiles(scilibpath, '*.a');
    for j = 1:size(lstlibs, '*')
      mdelete(scilibpath + filesep() + lstlibs(j));
    end
    bOK = %t;
  end
endfunction
// =============================================================================
