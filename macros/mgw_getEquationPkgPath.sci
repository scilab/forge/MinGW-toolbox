// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2023 - UTC - Stephane MOTTELET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function equationPath = mgw_getEquationPkgPath()
    equationPath = [];
    paths = strsplit(getenv("Path"),";");
    for p = paths'
        if ~isempty(strindex(p,"gcc\libexec\gcc\x86_64-w64-mingw32"));
            equationPath =  fullpath(p+"..\..\..\..\..\x86_64-w64-mingw32")
            break
        elseif ~isempty(strindex(p,"gcc\libexec\gcc\i686-pc-mingw32"))
            equationPath =  fullpath(p+"..\..\..\..\..\i686-pc-mingw32");
            break
        end        
    end
endfunction

