// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2011 - Allan CORNET
//
// Copyright (C) 2012 - 2016 - Scilab Enterprises
//
// This file is hereby licensed under the terms of the GNU GPL v2.0,
// pursuant to article 5.3.4 of the CeCILL v.2.1.
// This file was originally licensed under the terms of the CeCILL v2.1,
// and continues to be available under such terms.
// For more information, see the COPYING file which you should have received
// along with this program.
//=============================================================================
function scilablibrarieslist = dlwGetScilabLibraries()

    scilablibrarieslist = ["blasplus", ..
    "libf2c", ..
    "core", ..
    "core_f", ..
    "lapack", ..
    "output_stream", ..
    "dynamic_link", ..
    "integer", ..
    "optimization_f", ..
    "libjvm", ..
    "scilocalization", ..
    "linpack_f", ..
    "call_scilab", ..
    "time", ..
    "api_scilab", ..
    "libintl", ..
    "ast", ..
    "fileio", ..
    "io", ..
    "string", ..
    "threads", ..
    "sciconsole", ..
    "scilab_windows", ..
    "libmex"];
endfunction
//=============================================================================
