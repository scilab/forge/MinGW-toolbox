// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function bOK = mgw_convertScilabLibs()
  bOK = %T;
  scilabDlls = dlwGetScilabLibraries();
  
  if with_module('scicos') then
    scilabDlls = [scilabDlls, ..
                 'scicos', ..
                 'scicos-cli', ..
                 'scicos_f', ..
                 'scicos_blocks', ..
                 'scicos_blocks-cli', ..
                 'scicos_blocks_f'];
  end

  nbScilabDlls = size(scilabDlls, '*');

  if ( ilib_verbose() <> 0 ) then
    mprintf('Converting Libraries.\n');
  end

  for j = 1:nbScilabDlls

    if ( ilib_verbose() <> 0 ) then
        mprintf(gettext('Build lib%s.a\n'), scilabDlls(j));
    end

    ok = mgw_convertLibrary(SCI + filesep() + 'bin' + filesep() + scilabDlls(j) + getdynlibext(), mgw_getScilabLibPath());

    if ~ok then
      if ( ilib_verbose() <> 0 ) then
          mprintf(gettext('Conversion failed lib%s.a\n'), scilabDlls(j));
      end
      bOK = %F;
    end
  end

endfunction
// =============================================================================
