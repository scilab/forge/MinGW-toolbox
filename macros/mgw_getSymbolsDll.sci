// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// =============================================================================
function symbols = mgw_getSymbolsDll(dllname, bExport)
  symbols = [];
  if ~isdef('bExport') then
    bExport = %T;
  end
  if isfile(dllname) then
    if bExport then
      symbols = mgw_getExportedSymbols(dllname)
    else
      symbols = mgw_getImportedSymbols(dllname)
    end
  end
endfunction
// =============================================================================
function symbols = mgw_getImportedSymbols(dllname)
  symbols = [];
  [txt, bErr] = dos("""" + mgw_getArchBinPath() + filesep() + 'objdump"" -x -j .rdata ""' + dllname + """");
  if ~bErr then
    return
  end
  
  posExports = grep(txt,'Export Tables');
  if isempty(posExports) then
      posExports = grep(txt,'Sections');
  end
  txt = txt(grep(txt,'Import Tables') + 3 : posExports - 5);
  txt(txt == '') = [];
  txt(grep(txt, 'vma:')) = [];
  txt(grep(txt, 'DLL Name:') - 1) = [];
  
  symbols = list();
  idx = grep(txt, 'DLL Name:')
  for i = 1:size(idx, '*')
    symbols(i) = list()
    libName = strsubst(txt(idx(i)) , 'DLL Name: ', '');
    libName = stripblanks(libName);
    symbols(i)(1) = libName;
    
    if i == size(idx, '*') then
      ImportedSymbols = txt(idx(i) + 1: $)
    else
      ImportedSymbols = txt(idx(i) + 1: idx(i + 1))
    end
    
    ImportedSymbols = stripblanks(ImportedSymbols);
    ImportedSymbols = strrchr(ImportedSymbols, ' ');
    ImportedSymbols = stripblanks(ImportedSymbols);
    symbols(i)(2) = ImportedSymbols
  end
endfunction
// =============================================================================
function symbols = mgw_getExportedSymbols(dllname)
  symbols = list();
  [txt, bErr] = dos("""" + mgw_getArchBinPath() + filesep() + 'objdump"" -x ""' + dllname + """");
  if ~bErr then
    return
  end
  
  beginIdx = grep(txt,'Export Tables');
  if isempty(beginIdx) then
    beginIdx = 0;
  end

  endIdx = grep(txt,'PE File Base Relocations');
  if isempty(endIdx) then
    endIdx = 0;
  end
  
  txt = txt(beginIdx + 1 : endIdx - 1);
  txt(txt == '') = [];
  
  if isempty(txt) then
    return;
  end
  
  endline = grep(txt, 'The Function Table');
  if isempty(endline) then
    endline = -1;
  else 
    endline = endline - 1;
  end
  
  if endline == -1 then
    exportedSymbols = txt(grep(txt, '[Ordinal/Name Pointer] Table') + 1 : $);
  else
    exportedSymbols = txt(grep(txt, '[Ordinal/Name Pointer] Table') + 1 : endline);
  end

  if ~isempty(exportedSymbols) then
    exportedSymbols = stripblanks(exportedSymbols)
    exportedSymbols = strstr(exportedSymbols,'] ')
    exportedSymbols = strstr(exportedSymbols,' ')
    exportedSymbols = stripblanks(exportedSymbols)
    symbols(1) = fileparts(dllname, 'fname') + fileparts(dllname, 'extension');
    symbols(2) = exportedSymbols
  end
  
endfunction
// =============================================================================
