// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
foo=['void foo(double *a,double *b,double *c)';
     '{ *c = *a + *b; }'  ];

// we use TMPDIR for compilation 
chdir(TMPDIR); 
mputl(foo,'foo.c');

ilib_for_link(['foo'],'foo.c',[],"c");

// load the shared library 
exec loader.sce ;

//5+7 by C function
v = call('foo',5,1,'d',7,2,'d','out',[1,1],3,'d');
assert_checkequal(v, 5+7);
// ====================================================================
