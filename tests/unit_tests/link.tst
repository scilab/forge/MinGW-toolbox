// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// ====================================================================
// <-- CLI SHELL MODE -->

// ====================================================================
cd TMPDIR;
mkdir("link");
cd("link");

//Example of the use of ilib_for_link with  a simple C code
f1=['#include <math.h>'
    'void fooc(double c[],double a[], double *b,int *m,int *n)'
    '{'
    '   int i;'
    '   for ( i =0 ; i < (*m)*(*n) ; i++) '
    '     c[i] = sin(a[i]) + *b; '
    '}'];

mputl(f1, 'fooc.c');

//creating the shared library: a Makefile and a loader are 
//generated, the code is compiled and a shared library built.
ilib_for_link('fooc', 'fooc.c', [], "c"); 

// load the shared library 
exec('loader.sce'); 

link('show');

// call the new linked entry point
a = linspace(0, %pi, 10);
b = 5;
y1 = call('fooc', a, 2, 'd', b, 3, 'd', size(a,1), 4, 'i', size(a,2), 5, 'i', 'out', size(a), 1, 'd');
    
// check
assert_checkalmostequal(y1, sin(a) + b);

// ulink() all libraries
ulink();
