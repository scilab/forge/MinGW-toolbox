// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
cd TMPDIR;
mkdir("schur_test");
cd("schur_test");
A=diag([-0.9 -2 2 0.9]);
X=rand(A);
A=inv(X) * A * X;

// The same function in C
C=["int mytest(double *EvR, double *EvI) {"
   "if (*EvR * *EvR + *EvI * *EvI < 0.9025) return 1;"
   "else return 0; }";];
mputl(C,"TMPDIR/schur_test/mytest.c");

//build and link
lp=ilib_for_link("mytest", "mytest.c", [], "c");
link(lp, "mytest","c"); 

//run it
[U, dim, T] = schur(A, "mytest");
assert_checkequal(dim, 2);
// ====================================================================
