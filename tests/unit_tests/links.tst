// ====================================================================
// Allan CORNET
// DIGITEO 2010
// ====================================================================
// <-- CLI SHELL MODE -->
// <-- NO CHECK REF -->
// ====================================================================
curPath = pwd(); 
routines=["ext1f", "ext2f", "ext3f", "ext8f", "ext9f", "ext11f", "ext12f"];
copyfile(SCI+filesep()+'modules'+filesep()+'dynamic_link'+filesep()+'tests'+filesep()+'unit_tests'+filesep()+'externals.f', TMPDIR);
chdir(TMPDIR);
ilib_for_link(routines,'externals.f',[],"f");

// load the shared library 
exec loader.sce ;

// ====================================================================
//(very) simple example 1
// ====================================================================
a=[1,2,3];b=[4,5,6];n=3;
c=call('ext1f',n,1,'i',a,2,'d',b,3,'d','out',[1,3],4,'d');
assert_checkalmostequal(c, a + b);
// ====================================================================
//Simple example #2
// ====================================================================
a=[1,2,3];b=[4,5,6];n=3;
c=call('ext2f',n,1,'i',a,2,'d',b,3,'d','out',[1,3],4,'d');
assert_checkalmostequal(c, sin(a) + cos(b));
// ====================================================================
//Example #3
// ====================================================================
a=[1,2,3];b=[4,5,6];n=3;
c=call('ext3f','yes',1,'c',n,2,'i',a,3,'d',b,4,'d','out',[1,3],5,'d');
assert_checkalmostequal(c, sin(a) + cos(b));
c=call('ext3f','no',1,'c',n,2,'i',a,3,'d',b,4,'d','out',[1,3],5,'d');
assert_checkalmostequal(c, a + b);
// ====================================================================
//Example #8
// ====================================================================
//call ext8f argument function with dynamic link
yref=ode([1;0;0],0,[0.4,4],'ext8f');
// ====================================================================
//Example #9
// ====================================================================
//passing a parameter to ext9f routine by a list:
param=[0.04,10000,3d+7];    
y=ode([1;0;0],0,[0.4,4],list('ext9f',param));
assert_checkalmostequal(y, yref);
// ====================================================================
//Example #11
// ====================================================================
//sharing common data
a=1:10;
n=10;a=1:10;
call('ext11f',n,1,'i',a,2,'r','out',2);  //loads b with a
c=call('ext12f',n,1,'i','out',[1,10],2,'r');  //loads c with b
assert_checkalmostequal(c, a);
// ====================================================================
chdir(curPath);
// ====================================================================
